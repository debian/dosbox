dosbox (0.74-3-5) unstable; urgency=medium

  * Avoid accessing ncurses internals. Thanks to Sven Joachim for the
    patch! Closes: #1057553.
  * Convert THANKS to UTF-8.
  * Standards-Version 4.6.2, no change required.
  * Enable all hardening options.

 -- Stephen Kitt <skitt@debian.org>  Sat, 16 Dec 2023 12:09:02 +0100

dosbox (0.74-3-4) unstable; urgency=medium

  * Replace uses of variable strings as format strings with constants.
    Closes: #997144.
  * Drop the libncurses5-dev build-dependency in favour of libncurses-
    dev.
  * Standards-Version 4.6.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 26 Oct 2021 09:33:48 +0200

dosbox (0.74-3-3) unstable; urgency=medium

  * Apply upstream fix for key up event handling.

 -- Stephen Kitt <skitt@debian.org>  Wed, 03 Feb 2021 13:04:16 +0100

dosbox (0.74-3-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Archive, Repository.
  * Fix day-of-week for changelog entry 0.65-1.
  * Update standards version to 4.4.1, no changes needed.

  [ Stephen Kitt ]
  * Switch to debhelper compatibility level 13.
  * Unescape apostrophes in the man page.
  * Bump debian/watch version.
  * Standards-Version 4.5.1, no further change required.
  * Drop the configure patch from spelling-fixes.patch, it’s regenerated
    anyway.

 -- Stephen Kitt <skitt@debian.org>  Thu, 14 Jan 2021 18:33:15 +0100

dosbox (0.74-3-1) unstable; urgency=medium

  * New upstream release, including security fixes:
    - CVE-2019-7165: long lines in batch files would overflow the parsing
      buffer;
    - CVE-2019-12594: programs running inside DOSBox could access /proc.
    Closes: #931222.
  * Switch to debhelper compatibility level 12.

 -- Stephen Kitt <skitt@debian.org>  Thu, 18 Jul 2019 20:55:50 +0200

dosbox (0.74-2-3) unstable; urgency=medium

  * Enable the debugger and provide it in a separate dosbox-debug
    package.
  * Add a number of spelling fixes.
  * Update debian/copyright.
  * Standards-Version 4.3.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 22 Jan 2019 18:33:26 +0100

dosbox (0.74-2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/watch: Use https protocol

  [ Stephen Kitt ]
  * Apply upstream fix to allow grabbing the mouse again with recent
    X.org. Closes: #907271.

 -- Stephen Kitt <skitt@debian.org>  Sun, 23 Dec 2018 21:30:44 +0100

dosbox (0.74-2-1) unstable; urgency=medium

  * New upstream bug-fix release, obsoleting
    ftbfs624976_upstream_svn3691.patch, fix-ftbfs-format-security.patch,
    wine-move-z-mount-svn3736.patch, and
    wine-style-namemangling-svn3742.patch.
  * Adopt the package. Closes: #876772.
  * Allow hyphens in versions.
  * Point to the Salsa repository.
  * Switch to debhelper compatibility level 11.
  * Set “Rules-Requires-Root: no”.
  * Clean up whitespace and the package description.
  * Standards-Version 4.2.1, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Tue, 11 Sep 2018 09:15:15 +0200

dosbox (0.74-4.2) unstable; urgency=medium

  * non-maintainer upload
  * allow dosbox to satisfy dependencies from other architectures
    (Multi-Arch: foreign) - thanks Jens Reyer (Closes: #801615)

 -- Graham Inggs <ginggs@debian.org>  Tue, 13 Oct 2015 16:54:56 +0200

dosbox (0.74-4.1) unstable; urgency=medium

  * non-maintainer upload
  * improve interoperability with Wine (Closes: #789092)
    - allow Wine to move Z: mount (SVN r3736)
    - work with Wine-style name mangling (SVN r3742)
  * compile with -D_FILE_OFFSET_BITS=64 to fix mount failure
    with inodes > 2^32 - thanks Vladislav Naumov (Closes: #792886)

 -- Graham Inggs <ginggs@debian.org>  Wed, 07 Oct 2015 11:39:45 +0200

dosbox (0.74-4) unstable; urgency=medium

  * bump debhelper dependency and compat level to 9 to automatically enable
    hardening flags
  * restrict watch file to source tarballs
  * add debian/patches/fix-ftbfs-format-security.patch to fix build
    failure with -Werror=format-security
  * use autotools-dev
    - debian/control: add autotools-dev to Build-Depends
    - debian/rules: add --with autotools_dev to dh invocation
  * use canonical Vcs-* URLs
  * add Keywords to debian/dosbox.desktop, fix German comment spelling
  * apply patches by Rodrigo Silva to add larger multiple size icons and
    remove old pixmap icon (Closes: #756739) and remove old pre-XDG menu file
    (Closes: #619013)
  * switch debian/copyright to machine readable format
  * bump Standards-Version to 3.9.6 (no changes)

 -- Jan Dittberner <jandd@debian.org>  Thu, 09 Oct 2014 22:42:16 +0200

dosbox (0.74-3) unstable; urgency=low

  * Fix manpage bugs by updating debian/patches/fix_manpage_errors.patch
    (Closes: #644936)
  * Bump Standards-Version to 3.9.3 (no changes)
  * change Build-Depends, change from libpng12-dev to libpng-dev (Closes:
    #662303)

 -- Jan Dittberner <jandd@debian.org>  Mon, 05 Mar 2012 21:07:13 +0100

dosbox (0.74-2) unstable; urgency=low

  * Fix "FTBFS: ../../include/dos_inc.h:543:78: error: expected primary-
    expression before ',' token" add patch from upstream SVN as
    debian/patches/ftbfs624976_upstream_svn3691.patch (Closes: #624976)
  * fix another spelling error in man page in
    debian/patches/fix_manpage_errors.patch
  * Bump Standards-Version to 3.9.2 (No changes needed) in
    debian/control
  * fix description synopsis to not start with an article

 -- Jan Dittberner <jandd@debian.org>  Mon, 02 May 2011 21:20:37 +0200

dosbox (0.74-1) unstable; urgency=low

  * New upstream release.
  * remove debian/README.source because repacking is not required
  * remove version mangling from debian/watch
  * update debian/README.Debian, remove outdated note regarding removed
    files
  * remove Debian patches that have been applied upstream
   - debian/patches/fix_spelling_errors.patch
   - debian/patches/remove_fmopl_code.patch
   - debian/patches/remove_ymf262_code.patch
  * refresh debian/patches/fix_manpage_errors.patch
  * debian/control: update Standards-Version to 3.8.4, no changes needed

 -- Jan Dittberner <jandd@debian.org>  Fri, 14 May 2010 09:57:20 +0200

dosbox (0.73+dfsg1-1) unstable; urgency=low

  * remove src/hardware/ymf262.c, src/hardware/ymf262.h, add
    debian/patches/remove_ymf262_code.patch to remove dependencies on
    ymf262.{c,h}, update debian/patches/series and debian/README.source
    (Closes: #559443)
  * debian/copyright: refer to GPL-2 explicitly
  * debian/watch: add opts=dversionmangle to calculate correct upstream
    version
  * incorporate upstream suggestions:
    - remove debian/patches/bios_keyboard.cpp.patch, upstream uses
      putenv("SDL_DISABLE_LOCK_KEYS=1") in src/gui/sdlmain.cpp, update
      debian/patches/series
    - remove debian/dosbox.conf.example because dosbox creates a
      configuration file automatically if it does not exist, update
      debian/docs and debian/README.Debian

 -- Jan Dittberner <jandd@debian.org>  Sat, 05 Dec 2009 19:37:29 +0100

dosbox (0.73-2) unstable; urgency=low

  * debian/control:
    - remove quilt from Build-Depends because source format 3.0 (quilt)
      implies that already
  * debian/rules:
    - remove --with quilt
  * debian/README.source:
    - remove quilt notes

 -- Jan Dittberner <jandd@debian.org>  Tue, 01 Dec 2009 22:07:23 +0100

dosbox (0.73-1) unstable; urgency=low

  * New upstream release (LP: #382024, Closes: #530940)
  * Include fixes from NMU (Closes: #417159), thanks to James Vega
  * Switch from cdbs to debhelper 7 and quilt
  * remove old patches, applied upstream:
    debian/patches/dos_keyboard_layout.cpp.patch,
    debian/patches/dyn_fpu.h.patch, debian/patches/gcc-4.3.patch
    debian/patches/sdlmain.cpp.patch, debian/patches/xms.cpp.patch
  * debian/control:
    - new Maintainer (old Maintainer was inactive according to MIA db,
      Closes: #540821)
    - add Vcs-Git and Vcs-Browser fields
    - add ${misc:Depends} to Depends
    - update Standards-Version to 3.8.3 (added debian/README.source)
    - update Depends: remove cdbs, add quilt, update debhelper version to
      >=7.0.50
  * debian/compat:
    - bump to 7
  * debian/copyright:
    - update upstream copyright years, add package licensing and new
      maintainer information
  * debian/rules:
    - use dh7 rule instead of cdbs
  * add debian/watch (Closes: #530941)
  * remove non-free src/hardware/fmopl.c and src/hardware/fmopl.h,
    add patch debian/patches/remove_fmopl_code.patch to remove fmopl
    dependencies (Closes: #530824)
  * add debian/patches/series for quilt
  * refresh debian/patches/bios_keyboard.cpp.patch
  * add debian/README.source with instructions how to produce the
    original tarball
  * add debian/patches/fix_spelling_errors.patch to fix some spelling
    errors discovered by Lintian
  * add debian/patches/fix_manpage_errors.patch to fix warnings that
    lintian found in dosbox.1
  * add DEP-3 descriptions to patches
  * add debian/src/format for source format 3.0 (quilt)

 -- Jan Dittberner <jandd@debian.org>  Mon, 30 Nov 2009 21:47:41 +0100

dosbox (0.72-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * New patch which explicitly includes required headers to fix FTBFS with
    gcc-4.3.  Thanks to Joost Damad for the patch.  (Closes: #417159)

 -- James Vega <jamessan@debian.org>  Sun, 09 Mar 2008 12:35:53 -0400

dosbox (0.72-1) unstable; urgency=low

  * Include fixes from the 2 NMUs. (Closes: #415696, #417700, #375517, #361554)
  * 0.72 source appears fix to an old bug. (Closes: #432446)
  * Add a message to README.Debian concerning security (Closes: #458950)
  * Fix loading of french language file (Closes: #284907)
  * Integrate most of ubuntu .desktop changes: (Closes: #458089)
      - Fix name capitalization.
      - Remove extension from icon name.
        Change ca/de/en/es descriptions so that they refer to
        "applications" in general instead of "games".
  * Bump standards version to 3.7.3:
     - Move Homepage field to source stanza.
  * Changed to dephelper version 6
  * Added  Markus Schölzel as uploader

 -- Peter Veenstra <spiru@fmf.nl>  Sun, 02 Mar  2008 14:40:59 +0100

dosbox (0.72-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream release
  * Fixed .desktop and menu

 -- Markus Schölzel <makke@sidux.com>  Mon, 17 Sep 2007 18:26:23 +0200

dosbox (0.71-0.1) unstable; urgency=low

  * Non-maintainer upload.
  * New upstream release (Closes: #415696, #417700)
  * Added .desktop (Closes: #375517, #361554)

 -- Markus Schölzel <makke@sidux.com>  Tue, 31 Jul 2007 17:35:47 +0200

dosbox (0.65-1) unstable; urgency=low

  * New upstream version (Closes: #313240, #362928)
  * Include fixes from the 2 NMUs. (Closes: #319430, #356116)
  * Patch up various files to cvs versions to fix a few games.
  * Include documentation patch and send it upstream. (Closes: #301925)
  * Update package description. (Closes: #363298)
  * Work around debian specific SDL changes. (numlock/capslock)
  * Changed FSF address in debian/copyright
  * Standards-Version: 3.7.2 (No changes necessary)

 -- Peter Veenstra <spiru@fmf.nl>  Sat, 22 Jul 2006 12:10:59 +0100

dosbox (0.63-2.2) unstable; urgency=low

  * NMU as part of the GCC 4.1 transition.
  * Remove extra qualifications from C++ header file (Closes: #356116).

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 26 May 2006 23:40:18 +0200

dosbox (0.63-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control.in: Fix FTBFS bug by removing the obsolete
    alsa-headers from the Build-Depends (Closes: #319430).

 -- Roger Leigh <rleigh@debian.org>  Sun, 31 Jul 2005 15:34:15 +0100

dosbox (0.63-2) unstable; urgency=low

  * Fix compilation on amd64/gcc-4.0 (Closes: #285645)
  * Fix compilation on kfreebsd-gnu  (Closes: #278598)
  * Patched code up to cvs of 16 december 2004.
    Fixes various small bugs.
  * Improved documentation (Closes: #284500, #283570)
  * Build on unstable (Closes: #284032)
  * Change maintainer emailaddress.

 -- Peter Veenstra <spiru@fmf.nl>  Thu,  16 Dec 2004 16:00:08 +0100

dosbox (0.63-1) unstable; urgency=low

  * New stable upstream release. ( Closes: #275616, #183937 )

 -- Peter Veenstra <H.P.Veenstra@student.rug.nl>  Fri,  19 Nov 2004 11:27:08 +0100

dosbox (0.61-4) unstable; urgency=low

  * Fix compilation with GCC 3.4
  * Added more information about dynamic cpu core (Closes: #265319)
  * Changed CFLAGS to CXXFLAGS (Closes: #260026)
  * Disabled internal modem in configfile as it tries to open a protected port.

 -- Peter Veenstra <H.P.Veenstra@student.rug.nl>  Mon,  23 Aug 2004 13:56:08 +0100

dosbox (0.61-3) unstable; urgency=low

  * Fixed Overlay colour problems at PPC. (Closes: #235293)
  * Added quotes aroung menu tags to keep lintian silent.

 -- Peter Veenstra <H.P.Veenstra@student.rug.nl>  Tue,  2 Mar 2004 15:56:08 +0100

dosbox (0.61-2) unstable; urgency=low

  * Recompiled under unstable. (Closes: #231352)
  * Description lines were too long
  * Disabled opengl, unless it's specified in configfile.

 -- Peter Veenstra <H.P.Veenstra@student.rug.nl>  Fri,  13 Feb 2004 12:30:40 +0100

dosbox (0.61-1) unstable; urgency=low

  * New upstream release (Closes: #229896 )

 -- Peter Veenstra <H.P.Veenstra@student.rug.nl>  Wed, 04 Feb 2004 11:00:31 +0100

dosbox (0.60-3) unstable; urgency=low

  * Build against alsa (Closes: #216822 )
  * Added information about old configfiles (Closes: #217095 )
  * Added patch for G/S descriptor for ppc.
  * Fixed unintentional disabling of the internal modem and disney soundsource
  * Enabled Core-inline.
  * Added a sample configfile dosbox.conf.example

 -- Peter Veenstra <H.P.Veenstra@student.rug.nl>  Wed, 29 Oct 2003 15:00:31 +0100

dosbox (0.60-2) unstable; urgency=low

  * changed buildtime dependency from libsdl-net1.2 to libsdl-net1.2-dev

 -- Peter Veenstra <H.P.Veenstra@student.rug.nl>  Mon, 20 Oct 2003 19:46:32 +0200

dosbox (0.60-1) unstable; urgency=low

  * New upstream release
  * Added patches for ppc parityflag
  * patched core_normal in non-debug mode to handle opcode 106

 -- Peter Veenstra <spiru@fmf.nl>  Mon, 20 Oct 2003 19:46:10 +0200

dosbox (0.58-3) unstable; urgency=low

  * Fixed ppc problems with the internal filesystem ( Closes: #185012 )

 -- Peter Veenstra <H.P.Veenstra@student.rug.nl>  Tue, 23 Sep 2003 21:03:37 +0200

dosbox (0.58-2) unstable; urgency=low

  * GCC3.X transition.
  * Backported fixes to drivechache and the cdrom.

 -- Peter Veenstra <qbix@pcnwstage.phys.rug.nl>  Mon, 18 Aug 2003 14:04:17 +0200

dosbox (0.58-1) unstable; urgency=low

  * New upstream release

 -- Peter Veenstra <H.P.Veenstra@student.rug.nl>  Mon, 31 Mar 2003 13:36:23 +0200

dosbox (0.57cvs20030220-1) unstable; urgency=low

  * Initial Release. (Closes #155923)

 -- Peter Veenstra <H.P.Veenstra@student.rug.nl>  Thu,  20 Feb 2003 12:50:16 +0100
